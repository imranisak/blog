@component('mail::message')
    <h3>Your post, <b>{{$post['title']}}</b>, has been posted!</h3>
    @php
    //dd($post);
    @endphp

@component('mail::button', ['url' => '/posts/'.$post['id']])
Check out your post!
@endcomponent

Thanks,<br>
now go away :)
@endcomponent
