@extends('master')
@section('title', '{{$post->title}}')
@section('content')
    @php ($post->id==auth()->id()) ? $isOwner=true : $isOwner=false; @endphp
    <h1>{{$post->title}}</h1>
    <p>{{$post->content}}</p>
    <p>Owner: @php echo \App\User::FindOrFail($post->owner_id)->name; @endphp</p>

    <h3>Add a comment!</h3>
    <form action="/posts/{{$post->id}}/comment" method="post">
        @csrf
        <input type="text" name="comment" required>
        <button>Add comment!</button>
    </form>
    @include('partials.errors')

    <div style="width: 150px;">
        @if($post->comments->count())
            @foreach($post->comments as $comment)
                <p
                @if(!$comment->isApproved)
                    {{'style=background-color:rgba(255,0,0,0.5);padding:10px'}}
                @endif>
                    {{$comment->comment}}
                </p>
                @if($isOwner)
                    <form action="/comments/{{ $comment->id }}" method="post">
                        @csrf
                        @method('patch')
                        <button onclick="this.form.submit">@if($comment->isApproved)
                                                            Hide comment!
                                                            @else
                                                            Approve comment!
                                                            @endif
                        </button>
                    </form>
                    <form action="/comments/{{$comment->id}}" method="post">
                        @csrf
                        @method('delete')
                        <button onclick="this.form.submit">Delete comment!</button>
                    </form>
                @endif
                <sub>Posted by: @php echo \App\User::FindOrFail($comment->owner_id)->name; @endphp</sub>
                <hr>
            @endforeach
        @else
            <h3>No comments!</h3>
        @endif
    </div>

@endsection