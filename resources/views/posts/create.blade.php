@extends('master')
@section('title', 'Add a post')
@section('content')
    <h1>Add a post!</h1>
    <form action="/posts" method="post">
        {{csrf_field()}}
        <h3>Add a post</h3>
        <input type="text" name="title" placeholder="Post title" required value="{{old('title')}}"><br>
        <input type="textarea" name="content" placeholder="Enter post here!" required value="{{old('content')}}"><br>
        <button type="submit">Add post!</button>
    </form>
    @include('partials.errors')
@endsection