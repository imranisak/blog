@extends('master')
@section('title', 'All posts')
@section('content')
    <h1>All posts</h1>
<div id="allPosts" style="width: 500px">
    @foreach($posts as $post)
        <h1><a href="/posts/{{$post->id}}">{{$post->title}}</a></h1>
        @if($post->owner_id==auth()->id())
            <a href="/posts/{{$post->id}}/edit">Edit post</a>

        @endif
        <sub>Posted by<b> @php echo \App\User::FindOrFail($post->owner_id)->name; @endphp</b></sub>
        <hr>
    @endforeach
    {{$posts->links()}}

</div>


@endsection
