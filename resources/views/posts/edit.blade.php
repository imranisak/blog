@extends('master')
@section('title', 'Edit post')
@section('content')
    <h1>Edit a post!</h1>
    <form action="/posts/{{$post->id}}" method="post">
        {{csrf_field()}}
        {{method_field("patch")}}
        <input type="text" name="title" placeholder="Post title" value="{{$post->title}}" required><br>
        <input type="textarea" name="content" placeholder="Enter post here!" value="{{$post->content}}" required><br>
        <button type="submit">Save post!</button>
    </form>
        <form action="/posts/{{$post->id}}" method="post">
            @method('delete')
            @csrf
            <button type="submit">Delete post</button>
        </form>
    @include('partials.errors')

@endsection