<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostsController@home');

Route::resource('posts', 'PostsController');

//Comments
Route::patch('/comments/{comment}', 'CommentsController@update');
Route::get('/comments', 'CommentsController@test');
Route::post('/posts/{post}/comment', 'CommentsController@store');
Route::delete('/comments/{comment}', 'CommentsController@destroy');
//////////
Auth::routes();

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
