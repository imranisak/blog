<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    protected function index()
    {
        return view('projects.index');
    }
}
