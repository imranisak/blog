<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Posts;

class CommentsController extends Controller
{
    public function update(Comment $comment)
    {
        if($comment->isApproved) $comment->isApproved=false;
        else $comment->isApproved=true;
        $comment->save();
        return back();
    }

    public function store(Posts $post)
    {
        $validated=request()->validate(['comment'=>'required|min:3']);
        //dd(auth()->id());
        $validated['owner_id']=auth()->id();
        //dd($validated);
        $post->addComment($validated);
        return back();
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();
        return back();
    }
}
