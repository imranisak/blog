<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Mail\PostCreated;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Posts;


class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'home']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        return view('welcome');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts=Posts::where('id', '>',0)->orderBy('id', 'desc')->paginate(3);
        //dd($posts);
        return view('posts.index', compact('posts'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post=request()->validate([
            'title'=>['required', 'min:3'],
            'content'=>['required', 'min:3']
        ]);
        $post['owner_id']=auth()->id();
        Posts::create($post);
        //Also sends a mail
        return redirect('/posts');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Posts $post)
    {
        //dd($post);
        return view("posts.show", compact("post"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Posts $post)
    {
        abort_if($post->owner_id!==auth()->id(), 403);
        return view('posts.edit', compact("post"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Posts $post)
    {
        $validated=request()->validate([
            'title'=>['required', 'min:3'],
            'content'=>['required', 'min:3']
        ]);

        $post->update($validated);
        /*$post = Posts::findOrFail($id);
        $post->title=request('title');
        $post->content=request('content');
        $post->save();*/
        return redirect ('/posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posts $post)
    {
        $post->delete();
        return redirect('/posts');
    }
}
