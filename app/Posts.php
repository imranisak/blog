<?php

namespace App;

use App\Mail\PostCreated;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable=[
        'title',
        'content',
        'owner_id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($post)
        {
            $postWithID=Posts::where('owner_id', auth()->id())->orderBy('created_at', 'desc')->first();
            if($post['content']==$postWithID->content && $post['title']==$postWithID->title)//Checks if the posts are the same, juuuuust in case
            {
                $post['id']=$postWithID->id;
                \Mail::to(auth()->user()->email)->queue(
                    new PostCreated($post)
                );
            }
            else
            {
                throw new Exception("Internal error. Posts are not the same. Please, try again.");
                //IDK what to do here. Will add something later.
            }
        });

    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function addComment($comment)
    {
        $this->comments()->create($comment);
        /*return Comment::create([
            'posts_id' => $this->id,
            'comment' => $comment
        ]);*/
    }
}
